# NotifCompletude

Ce code accède aux données disponibles sur un serveur, en se connectant la première fois en demandant les identifiants (non sauvegardés) et en sauvegardant le token sous format txt. La connexion est maintenue grâce aux tokens qui sont automatiquement raffraîchis régulièrement.

