import firebase_admin
from firebase_admin import credentials
from firebase_admin import messaging
import datetime

# Load the Firebase credentials from a JSON file
cred = credentials.Certificate(
    "C:/Users/jules/Downloads/completudepush-firebase-adminsdk-7vhap-a89015b983.json")

# Initialize the Firebase app with the credentials
app = firebase_admin.initialize_app(cred)

# Specify the device registration token to which messages will be sent
registration_token = 'fWmq-J2iSZ--eHZMkybZMq:APA91bHMHjYwtoXl3vBMx1yWrk5avzjfAQFRu3HYEwQ8bKHglSEu6V8ezh00dE9Y7DjmXjT7ODTJ7FrkpiV_vxY1NMbdCQamCrDGqmj_Y9RswGJ5eK9VOnzQb_xEKLmiYbHjLnx_2eVn'


class Firebase():
    """
    A class for sending push notifications using the Firebase Cloud Messaging service.
    """

    @staticmethod
    def sendmsg(text):
        """
        Sends a push notification with the given text to the device identified by the registration token.

        Parameters:
            text (str): The text of the push notification.

        Returns:
            None
        """
        # Define the message payload
        message = messaging.Message(
            android=messaging.AndroidConfig(
                ttl=datetime.timedelta(seconds=3600),
                priority='high',
                notification=messaging.AndroidNotification(
                    title='Nouvelle offre',
                    body=f'{text}',
                    icon='ic_sync_black',
                    color='#6e9e2f'
                ),
            ),
            notification=messaging.Notification(
                title='Nouvelle offre',
                body=f'{text}',
            ),
            apns=messaging.APNSConfig(
                payload=messaging.APNSPayload(
                    aps=messaging.Aps(badge=42),
                ),
            ),
            token=registration_token,
        )
        # Send the message
        response = messaging.send(message)
        # Print a success message
        return(f'Successfully sent message:', {response})

    @staticmethod
    def sendmdp():
        """
        Sends a push notification requesting the user to enter their password.

        Parameters:
            None

        Returns:
            None
        """
        # Define the message payload
        message = messaging.Message(
            android=messaging.AndroidConfig(
                ttl=datetime.timedelta(seconds=3600),
                priority='high',
                notification=messaging.AndroidNotification(
                    title='Besoin du mdp',
                    body='Viens taper ton mdp',
                    icon='ic_sync_black',
                    color='#6e9e2f'
                ),
            ),
            token=registration_token,
        )
        # Send the message
        response = messaging.send(message)
        # Print a success message
        return(f'Successfully sent message:', {response})


print(Firebase.sendmsg('test'))
