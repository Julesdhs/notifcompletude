class Format():
    """
    exemple de d'argument : 
    
    Demandes = [{'IdDemande': 1459027, 'Ville': 'CHASNE SUR ILLET', 'DistanceKm': '15,92 km', 'CategorieAction': 0, 'InfoProposition': 2, 'LibelleMatiere': 'Math�matiques - Physique-Chimie', 'LibelleCombinaison': '2nde',
                'EstFille': True, 'LibelleFrequence': '1 fois/sem', 'LibelleDuree': '2h00', 'DetailPrix': '19,50�/h soit 39,00�/s�ance nets avant PAS', 'AdequatSemaine': False, 'AdequatWeekend': True}]
    
    """
    def msg(offre):
        """
        Returns a formatted message string for a given offer.

        Parameters:
        - offre (dict): A dictionary containing the offer details, such as distance, price, duration, etc.

        Returns:
        - (str): A formatted string with the offer details.

        """
        # Extract offer details from the dictionary
        distance = offre['DistanceKm']
        classe = offre['LibelleCombinaison']
        prixlist = [char for char in offre['DetailPrix'] if char.isdigit()]
        prix = str(prixlist[0]) + str(prixlist[1]) + ',' + str(prixlist[2]) + str(prixlist[3]) + \
            '/' + str(prixlist[4]) + str(prixlist[5]) + \
            ',' + str(prixlist[6]) + str(prixlist[7]) + '€'
        ville = offre['Ville']
        heures = offre['LibelleDuree']
        SE = ''
        WE = ''
        if offre['AdequatSemaine']:
            SE = 'SE'
        if offre['AdequatWeekend']:
            WE = 'WE'
        # Create and return the formatted message string
        msg = f"Offre à {distance} à {ville} en {classe} pour {prix} en {heures} dispo {SE}/{WE}"
        return(msg)

    def save(demandes):
        """
        Returns a formatted string with the IDs of the given demandes.

        Parameters:
        - demandes (list of dicts): A list of dictionaries containing the demandes details.

        Returns:
        - (str): A formatted string with the IDs of the demandes.

        """
        # Create an empty string
        string = ""
        nb = len(demandes)
        # If there are no demandes, return an empty string
        if nb == 0:
            return(string)
        # For each demande, add its ID to the string
        for offre in demandes:
            string += str(offre['IdDemande']) + '\n'
        # Return the formatted string
        return(string)
