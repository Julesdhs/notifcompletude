import unittest
from backend.formatmsg import Format


class TestFormat(unittest.TestCase):

    def test_msg(self):
        offre = {'IdDemande': 1459027, 'Ville': 'CHASNE SUR ILLET', 'DistanceKm': '15,92 km', 'CategorieAction': 0, 'InfoProposition': 2, 'LibelleMatiere': 'Mathématiques - Physique-Chimie', 'LibelleCombinaison': '2nde',
                 'EstFille': True, 'LibelleFrequence': '1 fois/sem', 'LibelleDuree': '2h00', 'DetailPrix': '19,50€/h soit 39,00€/séance nets avant PAS', 'AdequatSemaine': False, 'AdequatWeekend': True}
        result = Format.msg(offre)
        expected = "Offre à 15,92 km à CHASNE SUR ILLET en 2nde pour 19,50/39,00€ en 2h00 dispo /WE"
        self.assertEqual(result, expected)

    def test_save(self):
        demandes = [{'IdDemande': 1459027}, {
            'IdDemande': 1459028}, {'IdDemande': 1459029}]
        result = Format.save(demandes)
        expected = "1459027\n1459028\n1459029\n"
        self.assertEqual(result, expected)

    def test_save_empty(self):
        demandes = []
        result = Format.save(demandes)
        expected = ""
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
