import requests
from firebase import Firebase


class CheckOffres():

    def refreshtoken():
        with open('refreshtoken.txt', 'r', encoding="utf8") as refreshtoken:
            reftoken = str(refreshtoken.read())
            url = "https://sic.internetude.fr/Api/ProfesseurCompletude/TokenAuth"

            headers = {
                "accept": "application/json, text/plain, */*",
                "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
                "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"Windows\""
            }
            data = f"grant_type=refresh_token&refresh_token={reftoken}"

            response = requests.post(url, headers=headers, data=data)

            newtoken = (response.json()['access_token'])
            file = open("token.txt", "w")
            file.write(newtoken)
            file.close()

            newreftoken = (response.json()['refresh_token'])
            file = open("refreshtoken.txt", "w")
            file.write(newreftoken)
            file.close()

            return ()

    def token():
        Firebase.sendmdp()
        username = input('username')
        password = input('password')

        url = "https://sic.internetude.fr/Api/ProfesseurCompletude/TokenAuth"

        headers = {
            "accept": "application/json, text/plain, */*",
            "content-type": "application/x-www-form-urlencoded;charset=UTF-8",
            "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\""
        }
        data = f"username={username}&password={password}&grant_type=password"

        response = requests.post(url, headers=headers, data=data)

        refreshtoken = response.json()['refresh_token']
        file = open("refreshtoken.txt", "w")
        file.write(refreshtoken)
        file.close()

        return (response.json()['access_token'])

    def getoffres(token):

        url = "https://sic.internetude.fr/Api/ProfesseurCompletude/cap/search"

        headers = {
            "accept": "application/json, text/plain, */*",
            "authorization": f"Bearer {token}",
            "content-type": "application/json",
            "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\""
        }

        data = {
            "SessionRecherche": {
                "Id": 0,
                "IdAdresse": 1101420,
                "DispoSemaine": True,
                "DispoWeekEnd": True,
                "DispoVacance": False,
                "AvecVehicule": False,
                "AvecChien": True,
                "AvecChat": True
            },
            "MapBox": {
                "IdSessionRecherche": 0,
                "NordOuestLatitude": 48.31286471926788,
                "NordOuestLongitude": -2.0119465297852384,
                "SudEstLatitude": 47.86370708073213,
                "SudEstLongitude": -1.3395394702147616
            }
        }

        response = requests.post(url, headers=headers, json=data)

        return (response)

    def reqoffres():
        with open('token.txt', 'r', encoding="utf8") as token:
            t = str(token.read())
            response = CheckOffres.getoffres(t)
            if response.status_code == 200:
                print(200)
                return (response.json()['Demandes'])

            if response.status_code == 401:
                print(response.json())
                token = CheckOffres.token()
                file = open("token.txt", "w")
                file.write(token)
                file.close()
                return (CheckOffres.reqoffres())

            print(response.status_code)
            return (['réponse inattendue du serveur'])
            # faire un raise error etc pour la gestion ensuite, permettra d'envoyer une notif pour prévenir l'appli plus simplement en fonction des erreurs
