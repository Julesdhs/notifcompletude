from formatmsg import Format  # external module for formatting messages
from firebase import Firebase  # external module for sending push notifications
# external module for checking job offers
from requestoffres import CheckOffres
import schedule  # module for scheduling tasks
from threading import Thread  # module for running tasks in separate threads
from time import sleep  # module for delaying execution


def schedule_checker():
    """
    This function runs continuously in a separate thread and checks for scheduled tasks using the `schedule` module.
    It runs every 15 minutes (arbitrary) and checks for new job offers.
    """
    while True:
        schedule.run_pending()
        sleep(15 * 60)


def sendpush(offre):
    """
    This function takes a job offer as input, formats it using the `Format` module, and sends a push notification to Firebase.
    """
    text = Format.msg(offre)  # format job offer data
    Firebase.sendmsg(text)  # send push notification


def checkoffres():
    """
    This function checks for new job offers using the `CheckOffres` module.
    If a new offer is found, it formats the offer data, sends a push notification, and saves the offer ID to a file.
    """
    offres = CheckOffres.reqoffres()  # get job offer data
    text = [offre['IdDemande'] for offre in offres]  # extract offer IDs
    with open('offres.txt', 'r', encoding="utf8") as f:  # open file containing previous offer IDs
        listestr = f.read().splitlines()
        listeint = []
        if len(listestr) > 1:
            for item in listestr:
                listeint.append(int(item))
        for i in range(len(text)):
            if text[i] not in listeint:  # check if offer ID is new
                sendpush(offres[i])  # send push notification for new offer
    svg(offres)  # save offer data to file


def svg(offres):
    """
    This function takes job offer data as input and saves it to a file.
    """
    text = str(Format.save(offres))  # format offer data for saving to file
    file = open("offres.txt", "w")  # open file for writing
    file.write(text)  # save offer data to file
    file.close()


def refreshtoken():
    """
    This function refreshes the authentication token for the `CheckOffres` module.
    """
    CheckOffres.refreshtoken()


if __name__ == "__main__":
    # schedule tasks
    schedule.every().second.do(lambda: checkoffres())
    schedule.every(45).minutes.do(lambda: refreshtoken())

    # run schedule checker in separate thread
    Thread(target=schedule_checker).start()
