import unittest
from unittest import mock
from unittest.mock import patch
from backend.firebase import Firebase
import datetime

class TestFirebase(unittest.TestCase):

    def test_sendmsg(self):
        Firebase.sendmsg('test message')
        expected_message = {
            'android': {
                'ttl': datetime.timedelta(seconds=3600),
                'priority': 'high',
                'notification': {
                    'title': 'Nouvelle offre',
                    'body': 'test message',
                    'icon': 'ic_sync_black',
                    'color': '#6e9e2f'
                }
            },
            'notification': {
                'title': 'Nouvelle offre',
                'body': 'test message'
            },
            'apns': {
                'payload': {
                    'aps': {
                        'badge': 42
                    }
                }
            },
            'token': 'fWmq-J2iSZ--eHZMkybZMq:APA91bHMHjYwtoXl3vBMx1yWrk5avzjfAQFRu3HYEwQ8bKHglSEu6V8ezh00dE9Y7DjmXjT7ODTJ7FrkpiV_vxY1NMbdCQamCrDGqmj_Y9RswGJ5eK9VOnzQb_xEKLmiYbHjLnx_2eVn'
        }
        actual_message = mock_send.call_args[0][0].to_dict()
        self.assertEqual(actual_message, expected_message)

    @patch('backend.firebase.sendmdp')
    def test_sendmdp(self, mock_send):
        Firebase.sendmdp()
        expected_message = {
            'android': {
                'ttl': mock.ANY,
                'priority': 'high',
                'notification': {
                    'title': 'Besoin du mdp',
                    'body': 'Viens taper ton mdp',
                    'icon': 'ic_sync_black',
                    'color': '#6e9e2f'
                }
            },
            'token': 'fWmq-J2iSZ--eHZMkybZMq:APA91bHMHjYwtoXl3vBMx1yWrk5avzjfAQFRu3HYEwQ8bKHglSEu6V8ezh00dE9Y7DjmXjT7ODTJ7FrkpiV_vxY1NMbdCQamCrDGqmj_Y9RswGJ5eK9VOnzQb_xEKLmiYbHjLnx_2eVn'
        }
        mock_send.assert_called_with(mock.ANY)
        actual_message = mock_send.call_args[0][0].to_dict()
        self.assertEqual(actual_message, expected_message)


if __name__ == '__main__':
    unittest.main()
